#Module: Deadline
RewriteRule ^deadline(/?)$ %sitepath%/index.php?ns=deadline
RewriteRule ^deadline/backup(/?)$ %sitepath%/index.php?ns=deadline&controller=debug&action=backupxml
RewriteRule ^deadline/view/([0-9.]+)(/?)$ %sitepath%/index.php?ns=deadline&action=view&id=$1
RewriteRule ^deadline/dates(/?)$ %sitepath%/index.php?ns=deadline&controller=dates
RewriteRule ^deadline/dates/([0-9.]+)(/?)$ %sitepath%/index.php?ns=deadline&controller=dates&action=editfrm&id=$1
RewriteRule ^deadline/dates/newdate(/?)$ %sitepath%/index.php?ns=deadline&controller=dates&action=addfrm
RewriteRule ^deadline/rchart(/?)$ %sitepath%/index.php?ns=deadline&action=rchart

