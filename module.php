<?php
include "classes/deadline.php";
include "classes/dates.php";
include "config.php";
//$Yaps->Ulfs=new Ulfs(new db_connection($deadline_db_config));
$Yaps->Deadline=new Deadline($Yaps->db);
$Yaps->addModule('Deadline','deadline','Deadline dates management', 'Deadline dates visualizer');



function navmap_deadline(){
global $Yaps;
echo "<p>";
echo "<ul>";
echo "<li><a href=".$Yaps->config['site_path']."/deadline>Deadline</a>";

echo "<ul>";
echo "<li><a href=".$Yaps->config['site_path']."/deadline/rchart>RChart</a>";
echo "<li><a href=".$Yaps->config['site_path']."/deadline/dates>Dates</a>";
echo "<ul>";
echo "<li><a href=".$Yaps->config['site_path']."/deadline/dates/newdate>New date</a>";
echo "</ul>";

echo "</ul>";

echo "</ul>";
}


function current_date(){
return date(DATE_RFC2822);
}
