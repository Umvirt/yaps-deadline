<?php

class Deadline extends YapsModule {
function __construct($db){
$this->db=$db;
$this->dates=new DeadlineDates($db);
}


function backupxml(){
//return 'xxx';

$dom = new DOMDocument('1.0', 'utf-8');
$dom->formatOutput=true;
$root = $dom->createElement('module');


$dates = $dom->createElement('dates');

foreach($this->dates->items() as $v){
$date=$dom->createElement('dates');

$date->appendChild($dom->createElement('id',$v->id));
$date->appendChild($dom->createElement('date',$v->date));
$date->appendChild($dom->createElement('name',$v->name));
$date->appendChild($dom->createElement('description',$v->description));


$dates->appendChild($date);

}

$root->appendChild($dates);



$dom->appendChild($root);

return $dom->saveXML();


}


function backup($path){
$result=$this->backupxml();

file_put_contents($path.'/deadline.xml',$result);
}


}
